
export default {
  primary: '#7EE4EC',
  secondary: '#114B5F',
  background: '#FFD4CA',
  text: '#333333',
  fontFamily: 'Montserrat_400Regular',
};
