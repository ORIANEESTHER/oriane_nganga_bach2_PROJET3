# Application myNotes

Link to personal GITLAB:NGANGA Oriane / Oriane Nganga Bach2 PROJET3 · GitLab 

## Description
myNotes is a mobile note-taking application developed as part of project #3 for École Multimédia.

## Launch Guide

### Prerequisites
- Node.js installed on your computer. You can download it from [nodejs.org](https://nodejs.org/).

-Expo CLI installed on your computer. You can install it by running the following command in your terminal:

npm install -g expo-cli

-Expo Client application installed on your mobile device.

### Instructions

1. **Cloning the Git Repository:**
 -Clone the Git repository containing the code for the myNotes application on your computer:
   ```
   git clone [https://gitlab.com/ORIANEESTHER/oriane_nganga_bach2_PROJET3.git]
   ```

2. **Installing Dependencies:**
 -Navigate to the myNotes application directory in your terminal:
   ```
   cd ORIANE_NGANGA_BACH2_PROJET3
   ```
 - Install project dependencies by running the following command:
   ```
   npm install
   ```

3. **Launching the Application with Expo:**
 - Once the dependencies are installed, launch the application using Expo CLI:
   ```
   npm/yarn expo start
   ```
 -Scan the QR code displayed in your terminal with the Expo Client application on your mobile device to open the myNotes application.
