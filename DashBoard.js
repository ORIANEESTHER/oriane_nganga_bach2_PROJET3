import React, { useState, useEffect } from 'react';
import { View, Text, Button, ScrollView, StyleSheet } from 'react-native';
import NoteCard from './NoteCard';
import * as SQLite from 'expo-sqlite';

// Opening or creating the SQLite database
const db = SQLite.openDatabase('notes.db');
const DashBoard = ({ navigation }) => {

  // Declaring a Report to Store Notes
  const [notes, setNotes] = useState([]);

  // Utilisation de useEffect pour exécuter une action lors du montage du composant
  useEffect(() => {
    // Creation of the 'notes' table in the database if it does not already exist
    db.transaction(tx => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS notes (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, date TEXT, content TEXT, priority TEXT)',
        [null],
        (txObj, resultSet) => {
          console.log('init db', resultSet);
        }
      );
    });
    // Calling the fetchNotes function when mounting the component
    fetchNotes();
  }, []);

  // Using useEffect to Perform an Action When Switching Navigation
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", fetchNotes);
    return unsubscribe;
  }, [navigation]);


  // Defining the fetchNotes function to retrieve notes from the database
  const fetchNotes = () => {
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM notes',
        [null],
        (txObj, resultSet) => {
          console.log(resultSet.rows);
          setNotes(resultSet.rows._array);
        },
        (_, error) => {
          console.error(error);
        }
      );
    });

  };

  return (
    <View style={{ flex: 1, padding: 20 }}>
      <ScrollView contentContainerStyle={styles.scrollViewContent}>
        {notes.length > 0 ? (
          // Display Grade Cards if Notes are Available
          notes.map(note => (
            <NoteCard key={note.id} note={note} navigation={navigation} fetchNotes={fetchNotes} />
          ))
        ) : (
          // Display a message if no note is available
          <View style={styles.noNotesContainer}>
            <Text style={styles.noNotesText}>No notes available</Text>
          </View>
        )}
      </ScrollView>
      <Button title="Create a note" onPress={() => navigation.navigate('Form', { params: { fetchNote: fetchNotes } })} />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  scrollViewContent: {
    flexGrow: 1,
    justifyContent: 'center',
  },
  noNotesContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  noNotesText: {
    marginBottom: 20,
    fontSize: 18,
    textAlign: 'center',

  },
});

export default DashBoard;
