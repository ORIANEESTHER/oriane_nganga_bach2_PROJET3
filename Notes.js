import React, { useState } from 'react';
import { View, Text, Button, Modal, StyleSheet } from 'react-native';
import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('notes.db');

const Notes = ({ route, navigation }) => {
  // Retrieving route parameters, including the note to be displayed and the fetchNotes function
  const { note, fetchNotes } = route.params;

  // Status to show or hide the deletion confirmation window
  const [showConfirmation, setShowConfirmation] = useState(false);

  const handleDelete = () => {
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM notes WHERE id = ?',
        [note.id],
        () => {
          // Refresh notes after deletion
          fetchNotes();
          navigation.navigate('DashBoard');
        },
        (_, error) => {
          console.error(error);
        }
      );
    });
  };

  return (
    <View style={{ flex: 1, padding: 20 }}>
      {/* Viewing Note Details */}
      <Text style={{ fontWeight: 'bold', fontSize: 18 }}>{note.title}</Text>
      <Text>Date: {note.date}</Text>
      <Text>{note.content}</Text>
      <Text>Priority: {note.priority}</Text>
      <Button title="Update" onPress={() => navigation.navigate('Form', { note, fetchNotes })} />
      <Button title="Delete" onPress={() => setShowConfirmation(true)} />

      {/* Deletion Confirmation Window */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={showConfirmation}
        onRequestClose={() => {
          setShowConfirmation(false);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Are you sure you want to delete this note?</Text>
            <Button
              title="Cancel"
              onPress={() => setShowConfirmation(false)}
            />
            <Button
              title="Delete"
              onPress={() => {
                setShowConfirmation(false);
                handleDelete();
              }}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});

export default Notes;
