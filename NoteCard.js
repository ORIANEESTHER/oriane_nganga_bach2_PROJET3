import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('notes.db');
const NoteCard = ({ note, navigation, fetchNotes }) => {
  // Function to manage the click on a note

  const handlePress = () => {
    navigation.navigate('Note', { note, fetchNotes });
  };

  // Function to manage the deletion of a note
  const handleDelete = () => {
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM notes WHERE id = ?',
        [note.id],
        () => {
          // Refresh notes after deletion
          fetchNotes();
        },
        (_, error) => {
          console.error(error);
        }
      );
    });
  };

  return (
    <TouchableOpacity onPress={handlePress} style={{ marginBottom: 20, borderWidth: 1, borderColor: 'gray', padding: 10 }}>
      <Text style={{ fontWeight: 'bold' }}>{note.title}</Text>
      <Text>Date: {note.date}</Text>
      <Text>{note.content.substring(0, 50)}...</Text>
      <Text>Priority: {note.priority}</Text>
      <TouchableOpacity onPress={handleDelete} style={{ marginTop: 10 }}>
        <Text style={{ color: 'red' }}>Delete</Text>
      </TouchableOpacity>
    </TouchableOpacity>
  );
}

export default NoteCard;
