import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Form from "./Form";
import Notes from "./Notes";
import DashBoard from "./DashBoard";
import Theme from "./Theme";

// Creating the Navigation Stack
const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer theme={{ colors: { background: Theme.background }, fontFamily: 'Montserrat_400Regular' }}>
      {/* Navigation container with theme set */}
      < Stack.Navigator initialRouteName="Dashboard" >
        {/* Defining Navigation Screens */}
        < Stack.Screen name="DashBoard" component={DashBoard} />
        <Stack.Screen name="Note" component={Notes} />
        <Stack.Screen name="Form" component={Form} />
      </Stack.Navigator >
    </NavigationContainer >
  );
};

export default App;
