import React, { useState } from "react";
import { View, TextInput, Button } from "react-native";
import * as SQLite from "expo-sqlite";
import { Picker } from "@react-native-picker/picker";

const db = SQLite.openDatabase("notes.db");
const Form = ({ route, navigation }) => {

  // Retrieving route parameters, including the note to be changed if necessary
  const { note } = route.params || {};

  // Setting Statuses for Note Title, Content, and Priority
  const [title, setTitle] = useState(note ? note.title : "");
  const [content, setContent] = useState(note ? note.content : "");
  const [priority, setPriority] = useState(note ? note.priority : "Normal");

  // Note Recording Management Function
  const handleSave = () => {
    if (!title.trim() || !content.trim() || !priority.trim()) {
      // Checking if a field is empty
      alert("Please complete all fields.");
      return;
    }
    // Getting the current date in ISOString format
    const currentDate = new Date().toISOString();
    if (note) {
      // Processing to update an existing note
      db.transaction((tx) => {
        tx.executeSql(
          "UPDATE notes SET title = ?, content = ?, priority = ? WHERE id = ?",
          [title, content, priority, note.id],
          (_, result) => {
            console.log("Note updated:", result.rowsAffected);
            navigation.navigate("DashBoard");
          },
          (_, error) => {
            console.error("Error updating note:", error);
          }
        );
      });
    } else {
      // Processing to create a new note if note does not exist
      db.transaction((tx) => {
        tx.executeSql(
          "INSERT INTO notes (title, date, content, priority) values (?, ?, ?, ?)",
          [title, currentDate, content, priority],
          (_, result) => {
            console.log(result.rowsAffected);
            console.log("Note inserted with ID:", result.insertId);
            // Creating an Object Representing the New Note
            const newNote = {
              id: result.insertId,
              title,
              date: currentDate,
              content,
              priority,
            };
            navigation.navigate("DashBoard");
          },
          (_, error) => {
            console.error("Error inserting note:", error);
          }
        );
      });
    }
  };

  return (
    <View style={{ flex: 1, padding: 20 }}>
      {/* Input field for title content */}
      <TextInput
        placeholder="Title"
        value={title}
        onChangeText={(text) => setTitle(text)}
        style={{
          marginBottom: 10,
          borderWidth: 1,
          borderColor: "gray",
          padding: 10,
        }}
      />
      <TextInput
        placeholder="Content"
        value={content}
        onChangeText={(text) => setContent(text)}
        style={{
          marginBottom: 10,
          borderWidth: 1,
          borderColor: "gray",
          padding: 10,
        }}
        multiline
      />
      {/* Note Priority Selector */}
      <Picker
        selectedValue={priority}
        style={{ height: 50, width: "100%", marginBottom: 10 }}
        onValueChange={(itemValue, itemIndex) => setPriority(itemValue)}
      >
        <Picker.Item label="Important" value="important" color="red" />
        <Picker.Item label="Normal" value="Normal" color="orange" />
        <Picker.Item label="Think stupid" value="Think stupid" color="green" />
      </Picker>
      <Button title="SAVE" onPress={handleSave} />
    </View>
  );
};

export default Form;
